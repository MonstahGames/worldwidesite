const express = require('express');
const app = express();
const port = 3000;
const prefix = '/api/';

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

app.use(express.json());

var json = "";

app.get(prefix + 'get_status', (req, res) => {
    res.status(200).send(json);
});

var authToken = "null";
app.post(prefix + 'post_status', (req, res) => {
    if (req.header("Authorization") !== authToken) {
        return res.status(403).send();
    }
    json = req.body;
    res.json(req.body);
    console.log(req.body);
    res.status(200).send();
});

const client = new MongoClient(url, {
    useNewUrlParser: true
});
client.connect((err, db) => {
    if (err) throw err;
    var dbo = db.db("worldwidedb");
    dbo.collection("tokens").findOne({}, function (err, result) {
        if (err) throw err;
        authToken = result.token;
        db.close();
    });
});

app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});